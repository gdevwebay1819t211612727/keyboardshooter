import { Container, Text,TEXT_GRADIENT,ticker,utils,Sprite } from "pixi.js";
import KeyCodes from "../KeyCodes";
import KeyValues from "../KeyValues";
import EnemyLetters from "../EnemyLetters";
import BulletLetters from "../BulletLetters";

class TitleScreen extends Container{
    constructor(){
        super();
        this.interactive = true;
        this.playingBGM = true;
        this.elapsedTime = 0;
        this.ticker = ticker.shared;
        this.ticker.add(this.update,this);
        this.time = 0;
        this.enemyArray = [];
        this.playerArray = [];
        this.curKey = "A";
        this.alphabetArray = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
        this.lifeCounter = 5;
        this.scoreCounter = 0;

        var letters = "Hello";

        this.playText = new PIXI.Text(this.curKey,{
            fontFamily: 'Arial',
            fontSize: 20,
            fill: 0x00FFFF,
            align: 'center'
        });
        this.lifeText = new PIXI.Text("Life = ",{
            fontFamily: 'Arial',
            fontSize: 20,
            fill: 0xFFFFFF,
            align: 'center'
        });
        this.lifeText.position.set(10,5);
        this.scoreText = new PIXI.Text("Score = ",{
            fontFamily: 'Arial',
            fontSize: 20,
            fill: 0xFFFFFF,
            align: 'center'
        });
        this.scoreText.position.set(730,5);
        this.bgmInstance = createjs.Sound.play("assets/audio/Bgm.mp3",{
            volume : 0.5,
            loop : -1
            //delay: 100 //ma
        })
        this.pewSfx = createjs.Sound.play("assets/audio/Pew.mp3",{
            volume : 1.0,
            loop : 0
            //delay: 100 //ma
        })
        this.knockSfx = createjs.Sound.play("assets/audio/Knock.mp3",{
            volume : 1.0,
            loop : 0
            //delay: 100 //ma
        })
        this.clickSfx = createjs.Sound.play("assets/audio/Click.mp3",{
            volume : 1.0,
            loop : 0
            //delay: 100 //ma
        })
        this.addChild(this.playText);
        this.addChild(this.lifeText);
        this.addChild(this.scoreText);

        window.addEventListener("keyup",this.onKeyUp.bind(this));
        window.addEventListener("keydown",this.onKeyDown.bind(this));
        window.addEventListener("mousedown",this.mouseDown.bind(this));
        window.addEventListener("mousemove",this.mouseMove.bind(this));
        this.ticker.add(this.updateObjs,this);
    }

    onKeyDown(t){
        if(this.lifeCounter>0)
        {
            if(t.key == "ArrowUp")
            {
                this.playText.y -= 6;
                return;
            }
            if(t.key == "ArrowDown")
            {
                this.playText.y += 6;
                return;
            }
            if(t.key == " ")
            {
                this.pewSfx.play();
                let k = this.curKey;
                let n = k.toUpperCase();
                this.bullet = new BulletLetters(n,this.playText.x,this.playText.y,17,22);
                this.addChild(this.bullet);
                this.playerArray.push(this.bullet);
                return;
            }
            if(t.keyCode >= KeyCodes.KeyA && t.keyCode<= KeyCodes.KeyZ)
            {
                this.clickSfx.play();
                let i = t.key;
                this.playText.text = i.toUpperCase();
                this.curKey = t.key;
            }
        }
        
    }

    mouseDown(m){
        if(this.lifeCounter>0)
        {
            this.pewSfx.play();
            let k = this.curKey;
            let n = k.toUpperCase();
            this.bullet = new BulletLetters(n,this.playText.x,this.playText.y,17,22);
            this.addChild(this.bullet);
            this.playerArray.push(this.bullet);
        }
    }
    mouseMove(p){
        if(this.lifeCounter>0)
        {
            this.playText.y = p.y-20;
        }
        //console.log("yPos = "+this.playText.y);
    }
    onKeyUp(e){
        /*
        this.clickSfx.play();
        let i = e.key;
       this.playText.text = i.toUpperCase();
       this.curKey = e.key;
       */
    }
    updateObjs()
    {
        this.lifeText.text = "Life = " + this.lifeCounter;
        this.scoreText.text = "Score = " + this.scoreCounter;
        var randInt = 1;
        if(this.time>randInt)
        {
            this.spawnEnemies();
            var randDec = Math.random()*5;
            randInt = Math.floor(randDec);
            this.time = 0;
        }
        //console.log("Time =  "+ this.time);
        //console.log("Enem =  "+ this.enemyArray.length);
        //console.log("pla =  "+ this.playerArray.length);
        if(this.lifeCounter>0)
        {
            this.time +=this.deltaTime();
            this.updateObjectMovements();
            this.checkCollision();
            this.checkPosition();
        }
        if(this.lifeCounter == 0)
        {
            this.bgmInstance.stop();
        }
    }
    updateObjectMovements()
    {
        for(let i = 0;i<this.enemyArray.length;i++)
        {
            //console.log("EnemyKey =  "+ this.enemyArray[i].getKeyElement());
            this.enemyArray[i].UpdateEnemy();
        }
        for(let x = 0;x<this.playerArray.length;x++)
        {
            //console.log("PlayerKey =  "+ this.playerArray[x].getKeyElement());
            this.playerArray[x].UpdateBullet();
        }
        
    }
    spawnEnemies()
    {
        var randomDec = Math.random() * this.alphabetArray.length;
        var randInt = Math.floor(randomDec);
        var randomLetter = this.alphabetArray[randInt];
        //console.log("Rand Alp "+ randomLetter);
        this.enemy = new EnemyLetters(randomLetter,850,Math.random() * 450,17,22);
        this.addChild(this.enemy);
        this.enemyArray.push(this.enemy);
    }
    checkCollision()
    {
        for(let i = 0;i<this.enemyArray.length;i++)
        {
            for(let x = 0;x<this.playerArray.length;x++)
            {
                this.isCollide = this.onCollision(this.enemyArray[i],this.playerArray[x]);
                if(this.enemyArray[i].getKeyElement() == this.playerArray[x].getKeyElement()&&this.isCollide)
                {
                    this.knockSfx.play();
                    this.removeChild(this.enemyArray[i]);
                    this.enemyArray.splice(i,1);
                    this.removeChild(this.playerArray[x]);
                    this.playerArray.splice(x,1);
                    this.scoreCounter+=50;
                }
                else if(this.isCollide)
                {
                    this.removeChild(this.playerArray[x]);
                    this.playerArray.splice(x,1);
                }
            }
        }
    }
    deltaTime()
    {
        return this.ticker.elapsedMS/1000;
    }
    checkPosition()
    {
        //console.log("BEnem =  "+ this.enemyArray.length);
        //console.log("Bpla =  "+ this.playerArray.length);
        if(this.enemyArray.length>=0)
        {
            for(let i = 0;i<this.enemyArray.length;i++)
            {
                //console.log("enemyPos =  "+ this.enemyArray[i].retPosition());
                if(this.enemyArray[i].retXPosition()<=0)
                {
                    this.removeChild(this.enemyArray[i]);
                    this.lifeCounter -= 1;
                    this.enemyArray.splice(i,1);
                }
            }
        }
        if(this.playerArray.length>=0)
        {
            for(let j = 0;j<this.playerArray.length;j++)
            {
                //console.log("PlayPos =  "+ this.playerArray[j].retPosition());
                if(this.playerArray[j].retXPosition()>=850)
                {
                    //console.log("PlayPos =  "+ this.playerArray[j].retPosition());
                    this.removeChild(this.playerArray[j]);
                    this.playerArray.splice(j,1);
                }
            }
        } 
        if(this.playText.y<=0)
        {
            this.playText.y = 0;
        }  
        else if(this.playText.y>=480)
        {
            this.playText.y = 480;
        }
    }
    onCollision(objE,objP)
    {
        var letHeight = 22;
        var letWidth = 18;
        var objeX = objE.retXPosition();
        var objeY = objE.retYPosition();
        var objpX = objP.retXPosition();
        var objpY = objP.retYPosition();
        //console.log(objE.retXPosition());
        //console.log(objeX+" "+objeY+" "+objpX+" "+objpY);
        if(objeX < objpX + letWidth &&
            objeX + letWidth > objpX &&
            objeY < objpY + letHeight &&
            objeY + letHeight > objpY)
        {
            console.log("Colided");
            return true;
        }
        else
        {
            return false;
        }
    }
}

export default TitleScreen;