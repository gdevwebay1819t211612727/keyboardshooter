import { Container, Text,TEXT_GRADIENT,ticker,utils,Sprite } from "pixi.js";
class BulletLetters extends Container{
    constructor(keyBull,xPos,yPos,w,h){
		super();
        this.ticker = ticker.shared;
        this.speed = 50;
		this.xOrigin = xPos;
		this.yOrigin = yPos;
		this.eWidth = w;
        this.eHeight = h;
        this.keyElem = keyBull;
		//console.log("w = "+this.eWidth+", h = "+this.eHeight);
		//console.log("x = "+this.xOrigin+", y = "+this.yOrigin);
		this.rectangle = new PIXI.Graphics();
		this.rectangle.beginFill(0x00FFFF);
		this.rectangle.drawRect(this.xOrigin,this.yOrigin,this.eWidth,this.eHeight);
		this.rectangle.endFill();
		
		this.numbText = new PIXI.Text(this.keyElem,{
            fontFamily: 'Arial',
            fontSize: 20,
            fill: 0x000000,
            align: 'center'
        });
		this.numbText.position.set(this.xOrigin,this.yOrigin);
		
		this.addChild(this.rectangle);
		this.addChild(this.numbText);
		
	}
	UpdateBullet(){
		this.numbText.x += this.speed*this.deltaTime();
		this.rectangle.x += this.speed*this.deltaTime();
	}
	onCollision(obj){
		if(this.rectangle.x < obj.x + obj.eWidth &&this.rectangle.x + this.rectangle.eWidth > obj.x &&
			this.rectangle.y < obj.y + obj.eHeight &&this.rectangle.y + this.rectangle.eHeight > obj.y)
		{
			return true;
		}
		else 
		return false;
    }
    retXPosition()
	{
		return this.numbText.x;
    }
    retYPosition()
	{
		return this.numbText.y;
	}
	getKeyElement()
	{
		return this.keyElem;
	}
	deltaTime()
    {
        return this.ticker.elapsedMS/1000;
    }
}
export default BulletLetters;