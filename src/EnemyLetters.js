import { Container, Text,TEXT_GRADIENT,ticker,utils,Sprite } from "pixi.js";
class EnemyLetters extends Container{
    constructor(keyEnem,xPos,yPos,w,h){
		super();
        this.ticker = ticker.shared;
        this.speed = -50;
		this.xOrigin = xPos;
		this.yOrigin = yPos;
		this.eWidth = w;
		this.eHeight = h;
		this.keyElem = keyEnem;
		//console.log("w = "+this.eWidth+", h = "+this.eHeight);
		//console.log("x = "+this.xOrigin+", y = "+this.yOrigin);
		this.rectangle = new PIXI.Graphics();
		this.rectangle.beginFill(0xFF0000);
		this.rectangle.drawRect(this.xOrigin,this.yOrigin,this.eWidth,this.eHeight);
		this.rectangle.endFill();
		
		this.enemText = new PIXI.Text(this.keyElem,{
            fontFamily: 'Arial',
            fontSize: 20,
            fill: 0x000000,
            align: 'center'
        });
		this.enemText.position.set(this.xOrigin,this.yOrigin);
		
		this.addChild(this.rectangle);
		this.addChild(this.enemText);
		
	}
	UpdateEnemy(){
		this.enemText.x += this.speed*this.deltaTime();
		this.rectangle.x += this.speed*this.deltaTime();
	}
	onCollision(obj){
		console.log(" Enemyletter "+"obj y = "+obj.rectangle.y+", obj.x = "+obj.rectangle.x);
		//console.log(" Enemyletter "+"enem W = "+this.eWidth+", enem H = "+this.eHeight);
		if(this.enemText.x < obj.x + obj.eWidth &&this.enemText.x + this.eWidth > obj.x &&
			this.enemText.y < obj.y + obj.eHeight &&this.enemText.y + this.eHeight > obj.y)
		{
			console.log("Collided");
			return true;
		}
		else 
		console.log("Not Collided");
		return false;
	}
	retXPosition()
	{
		return this.enemText.x;
	}
	retYPosition()
	{
		return this.enemText.y;
	}
	getKeyElement()
	{
		return this.keyElem;
	}
	deltaTime()
    {
        return this.ticker.elapsedMS/1000;
    }
}
export default EnemyLetters;